package com.example.reactivesecurity.dtoShite.auth

data class Request(
    val username: String,
    val password: String
)
