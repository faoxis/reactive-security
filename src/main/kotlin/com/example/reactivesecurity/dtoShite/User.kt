package com.example.reactivesecurity.dtoShite

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import java.util.stream.Collectors

data class User (
    private val username: String,
    private val password: String,
    val enabled: Boolean,
    val roles: List<Role>
): UserDetails {
    override fun getAuthorities(): MutableCollection<out GrantedAuthority> =
        roles
            .map { authority -> SimpleGrantedAuthority(authority.name) }
            .toMutableList()

    override fun getPassword(): String = password

    override fun getUsername(): String = username

    override fun isAccountNonExpired(): Boolean {
        return false
    }

    override fun isAccountNonLocked(): Boolean {
        return false
    }

    override fun isCredentialsNonExpired(): Boolean {
        return false
    }

    override fun isEnabled(): Boolean {
        return enabled
    }
}
