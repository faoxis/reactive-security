package com.example.reactivesecurity.config.security

import org.springframework.security.authentication.ReactiveAuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.stereotype.Component
import reactor.core.publisher.Mono

@Component
class AuthenticationManager(
    private val jwtUtil: JWTUtil
) : ReactiveAuthenticationManager {


    /**
     *  TODO: Понять как не пользоваться just
     */
    override fun authenticate(authentication: Authentication): Mono<Authentication> {
        val token = authentication.credentials.toString()
        val username = jwtUtil.getUsername(token)
        return Mono.just(jwtUtil.validateToken(token))
            .filter { valid -> valid }
            .switchIfEmpty(Mono.empty())
            .map {
                val claims = jwtUtil.getAllClaims(token)
                val roles: MutableList<String> = claims
                    .get("role", MutableList::class.java) as MutableList<String>

                UsernamePasswordAuthenticationToken(
                    username,
                    null,
                    roles.map(::SimpleGrantedAuthority)
                )

            }


    }

}