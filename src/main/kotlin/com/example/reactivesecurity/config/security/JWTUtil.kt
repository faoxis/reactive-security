package com.example.reactivesecurity.config.security

import com.example.reactivesecurity.dtoShite.User
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import java.util.*
import kotlin.collections.HashMap

@Component
class JWTUtil(
    @Value("\${jwt.salt}") private val secret: String,
    @Value("\${jwt.expiration}") private val expiration: Int,
) {

    private val key = Keys.hmacShaKeyFor(secret.toByteArray())

    fun getAllClaims(token: String): Claims {
        return Jwts
            .parserBuilder()
            .setSigningKey(key)
            .build()
            .parseClaimsJws(token)
            .body
    }

    fun getUsername(token: String): String {
        return getAllClaims(token).subject
    }

    fun getExpirationDate(token: String): Date {
        return getAllClaims(token).expiration
    }

    private fun tokenExpired(token: String): Boolean {
        val expiration = getExpirationDate(token)
        return expiration.before(Date())
    }

    private fun generateToken(claims: Map<String, Any>, username: String): String {
        val now = Date()
        val expirationDate = Date(now.time + (expiration.toLong() * 1000) )

        return Jwts.builder()
            .setClaims(claims)
            .setSubject(username)
            .setIssuedAt(now)
            .setExpiration(expirationDate)
            .signWith(key)
            .compact()
    }

    fun validateToken(token: String): Boolean {
        return !tokenExpired(token)
    }

    fun generateToken(user: User): String {
        val claims = HashMap<String, Any>()
        claims["role"] = user.roles
        return generateToken(claims, user.username)
    }

}