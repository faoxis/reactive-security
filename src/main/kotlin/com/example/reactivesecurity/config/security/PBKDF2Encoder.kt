package com.example.reactivesecurity.config.security

import org.springframework.beans.factory.annotation.Value
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Component
import java.util.*
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.PBEKeySpec

@Component
class PBKDF2Encoder(
    @Value("\${jwt.secret}") private val secret: String,
    @Value("\${jwt.iteration}") private val iteration: Int,
    @Value("\${jwt.keylength}") private val keylength: Int
) : PasswordEncoder {

    override fun encode(rawPassword: CharSequence): String {
        val result = SecretKeyFactory
            .getInstance("PBKDF2WithHmacSHA512")
            .generateSecret(PBEKeySpec(
                rawPassword.toString().toCharArray(),
                secret.toByteArray(),
                iteration,
                keylength
            ))
            .encoded

        return Base64.getEncoder().encodeToString(result)
    }

    override fun matches(rawPassword: CharSequence, encodedPassword: String): Boolean {
        return encode(rawPassword) == encodedPassword
    }

}