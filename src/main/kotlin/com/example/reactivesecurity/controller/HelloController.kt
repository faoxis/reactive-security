package com.example.reactivesecurity.controller

import com.example.reactivesecurity.config.security.JWTUtil
import com.example.reactivesecurity.dtoShite.HelloDto
import com.example.reactivesecurity.dtoShite.auth.Request
import com.example.reactivesecurity.dtoShite.auth.Response
import com.example.reactivesecurity.service.UserService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono

@RestController
class HelloController(
    private val jwtUtil: JWTUtil,
    private val passwordEncoder: PasswordEncoder,
    private val userService: UserService
) {

    @GetMapping("/hello")
    fun sayHello() = HelloDto("Hello!")

    @PostMapping("/login")
    fun login(@RequestBody request: Request): Mono<ResponseEntity<Response>> {
        return userService.findByUsername(request.username)
            .filter { details -> passwordEncoder.encode(request.password) == details.password }
            .map { details -> ResponseEntity.ok(Response(jwtUtil.generateToken(details))) }
            .switchIfEmpty(Mono.just(ResponseEntity.status(HttpStatus.UNAUTHORIZED).build()))
    }
}
