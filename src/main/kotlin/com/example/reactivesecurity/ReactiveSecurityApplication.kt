package com.example.reactivesecurity

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ReactiveSecurityApplication

fun main(args: Array<String>) {
	runApplication<ReactiveSecurityApplication>(*args)
}
