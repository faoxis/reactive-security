package com.example.reactivesecurity.service

import com.example.reactivesecurity.dtoShite.Role
import com.example.reactivesecurity.dtoShite.User
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono

@Service
class UserService {

    private val data: Map<String, User> = mutableMapOf<String, User>().apply {
        put(
            "user",
            User(
                "user",
                "cBrlgyL2GI2GINuLUUwgojITuIufFycpLG4490dhGtY=",
                true,
                listOf(Role.ROLE_USER)
            )
        )
        put(
            "admin",
            User(
                "admin",
                "dQNjUIMorJb8Ubj2+wVGYp6eAeYkdekqAcnYp+aRq5w=",
                true,
                listOf(Role.ROLE_ADMIN)
            )
        )
    }

    fun findByUsername(username: String): Mono<User> {
        return Mono.justOrEmpty(data[username])
    }

}